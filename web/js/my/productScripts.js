function deleteProduct(id, name) {
  if (confirm("Удалить " + name + "?")) {
    $.post("product/" + id + "/delete")
      .done(function () {
        location.reload();
      })
  }
}
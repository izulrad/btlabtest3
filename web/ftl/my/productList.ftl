[#ftl]
[#import "spring.ftl" as spring]
[#import "macros/template.ftl" as template]
[#escape x as x?html]

<!DOCTYPE html>
<html lang="ru">

  [@template.head assetPrefix="."/]

<body>

<div class="container">

    <div class="page-header">
        <h1>Список товаров</h1>
    </div>

[#--NAV BAR--]
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

            [#--ADD PRORUCT--]
                <ul class="nav navbar-nav navbar-left">
                    <li><a class="navbar-brand" href="product/new">Добавить продукт</a></li>
                </ul>

            [#--SEARCH BY NAME--]
                <form class="navbar-form navbar-left" method="GET">
                  [#if RequestParameters.size??]
                      <input hidden name="size" value="${RequestParameters.size}" readonly/>
                  [/#if]
                    <div class="form-group">
                        <input id="search-by-name" type="text" name="name" class="form-control" required
                               placeholder="Искать по названию" value="${RequestParameters.name!}">
                    </div>
                    <button type="submit" class="btn btn-default">Поиск</button>
                    <button type="submit" class="btn btn-default" form="search-clear-form">Очистить</button>
                </form>

            [#--CLEAR SEARCH--]
                <form id="search-clear-form" hidden method="GET">
                  [#if RequestParameters.size??]
                      <input hidden name="size" value="${RequestParameters.size}" readonly/>
                  [/#if]
                </form>

            [#--ITEMS PER PAGE--]
                <form class="navbar-form navbar-right" method="GET">
                  [#if RequestParameters.name??]
                      <input hidden name="name" value="${RequestParameters.name}" readonly/>
                  [/#if]

                    <label for="size">Записей на страницу</label>

                    <div class="form-group">
                        <input id="page-size" type="number" name="size" class="form-control" required
                               value="${RequestParameters.size!20}">
                    </div>
                    <button type="submit" class="btn btn-default">Показать</button>
                </form>

            </div>[#--navbar-collapse--]
        </div>[#--container-fluid--]
    </nav>

[#--PRODUCT TABLE--]
    <table class="table table-bordered table-striped ">
        <thead>
        <tr>
            <th>#</th>
            <th><a [#if pagination.sortByName??] href="${pagination.sortByName}" [/#if]>Наименование</a></th>
            <th>Описание</th>
            <th>Дата добавления</th>
            <th>Номер ячейки</th>
            <th>Зарезервирован</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
          [#list products as product]
          <tr>
              <td>${product.id}</td>
              <td><a href="product/${product.id}">${product.name}</a></td>
              <td>${product.description}</td>
              <td>${product.createdDate?date}</td>
              <td>${product.placeStorage}</td>
              <td>${product.reserved?then("Да", "Нет")}</td>
              <td>
                  <button class="btn btn-link" onclick="deleteProduct(${product.id},'${product.name}')">
                      удалить
                  </button>
              </td>
          </tr>
          [/#list]
        </tbody>
    </table>

[#--PAGINATION--]
  [#if pagination.previous?? || pagination.next??]
      <nav class="text-center">
          <ul class="pagination">

              <li [#if !pagination.previous??] class="disabled" [/#if]>
                  <a [#if pagination.previous??] href="${pagination.previous.link!}" [/#if] aria-label="Previous">
                      <span aria-hidden="true">&laquo;</span>
                  </a>
              </li>

            [#list pagination.pages as page]
                <li [#if page.active] class="active" [/#if]>
                    <a href="${page.link}">${page.name}</a>
                </li>
            [/#list]

              <li  [#if !pagination.next??] class="disabled" [/#if]>
                  <a [#if pagination.next??] href="${pagination.next.link}" [/#if] aria-label="Next">
                      <span aria-hidden="true">&raquo;</span>
                  </a>
              </li>

          </ul>
      </nav>
  [/#if]

  [@template.script assetPrefix="."/]

</div>[#--conteiner--]

</body>
</html>
[/#escape]
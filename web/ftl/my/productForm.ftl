[#ftl]
[#import "spring.ftl" as spring]
[#import "macros/template.ftl" as template]
[#escape x as x?html]

<!DOCTYPE html>
<html lang="ru">

  [@template.head assetPrefix=".."/]

<body>

<div class="container">

    <div class="page-header">
        <h1>${product.name!"Новый продукт"}</h1>
    </div>

    <form action="save" method="POST">

      [@spring.formInput 'product.id' 'hidden readonly' 'text'/]

        <div class="form-group row">
            <label for="name" class="col-md-2 form-control-label">Название</label>

            <div class="col-md-4">
              [@spring.formInput
              'product.name'
              'class="form-control" placeholder="Название продукта" maxlength="512" required'
              'text'/]
            </div>
        </div>

        <div class="form-group row">
            <label for="description" class="col-md-2 form-control-label">Описание</label>

            <div class="col-md-4">
              [@spring.formTextarea
              'product.description'
              'class="form-control" placeholder="Описание продукта" maxlength="1024" required rows="4" cols="10"'/]
            </div>
        </div>

        <div class="form-group row">
            <label for="createdDate" class="col-md-2 form-control-label">Дата добавления</label>

            <div class="col-md-2">
              [@spring.formInput
              'product.createdDate'
              'class="form-control" placeholder="Дата добавления" required'
              'text'/]
            </div>
        </div>

        <div class="form-group row">
            <label for="placeStorage" class="col-md-2 form-control-label">Номер ячейки</label>

            <div class="col-md-2">
              [@spring.formInput
              'product.placeStorage'
              'class="form-control" id="placeStorage" placeholder="Номер ячейки" required min="0"'
              'number'/]
            </div>
        </div>

        <div class="form-group row">
            <label for="reserved" class="col-md-2 form-control-label">Зарезирвирован</label>

            <div class="col-md-2">
                <label>
                  [@spring.formCheckbox
                  'product.reserved'
                  'name=reserved'/]
                </label>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-offset-2 col-md-10">
                <button type="submit" class="btn btn-success">Сохранить</button>
                <button type="button" class="btn btn-default" onclick="history.back();">Отменить</button>
            </div>
        </div>

    </form>

  [@template.script assetPrefix=".."/]

    <script>
        $("#createdDate").datepicker({
            language: "ru",
            autoclose: true,
            todayHighlight: true
        });
    </script>

</div>[#--conteiner--]

</body>
</html>
[/#escape]
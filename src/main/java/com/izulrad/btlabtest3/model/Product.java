package com.izulrad.btlabtest3.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

@Entity
@Table(name = "PRODUCTS")
public class Product {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private long id;

    @Column(name = "NAME", length = 512, nullable = false)
    private String name;

    @Column(name = "DESCRIPTION", length = 1024, nullable = false)
    private String description;

    @DateTimeFormat(pattern = "dd.MM.yyyy")
    @Column(name = "CREATED_DATE", nullable = false)
    private Date createdDate;

    @Column(name = "PLACE_STORAGE", nullable = false)
    private int placeStorage;

    @Column(name = "RESERVED", nullable = false)
    private boolean reserved;

    public Product() {
    }

    /*For test*/
    public Product(long id, String name, String description, Date createdDate, int placeStorage, boolean reserved) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.createdDate = createdDate;
        this.placeStorage = placeStorage;
        this.reserved = reserved;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public int getPlaceStorage() {
        return placeStorage;
    }

    public void setPlaceStorage(int placeStorage) {
        this.placeStorage = placeStorage;
    }

    public boolean isReserved() {
        return reserved;
    }

    public void setReserved(boolean reserved) {
        this.reserved = reserved;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;
        if (id != product.id) return false;
        if (placeStorage != product.placeStorage) return false;
        if (reserved != product.reserved) return false;
        if (name != null ? !name.equals(product.name) : product.name != null) return false;
        if (description != null ? !description.equals(product.description) : product.description != null) return false;
        if (createdDate != null && product.createdDate != null) {
            LocalDate date1 = createdDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            LocalDate date2 = product.createdDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

            return date1.compareTo(date2) == 0;
        }
        return createdDate == product.createdDate;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (createdDate != null ? createdDate.hashCode() : 0);
        result = 31 * result + placeStorage;
        result = 31 * result + (reserved ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", createdDate=" + createdDate +
                ", placeStorage=" + placeStorage +
                ", reserved=" + reserved +
                '}';
    }
}

package com.izulrad.btlabtest3.service.impl;

import com.izulrad.btlabtest3.model.Product;
import com.izulrad.btlabtest3.repository.ProductRepository;
import com.izulrad.btlabtest3.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService {
    static final Logger LOG = LoggerFactory.getLogger(ProductServiceImpl.class);

    private ProductRepository productRepository;

    @Autowired
    public void setProductRepository(ProductRepository productRepository) {
        LOG.debug("setProductRepository");

        this.productRepository = productRepository;
    }

    @Override
    public Product save(Product product) {
        LOG.debug("save " + product);

        return productRepository.save(product);
    }

    @Override
    public void delete(long id) {
        LOG.debug("delete id=" + id);

        productRepository.delete(id);
    }

    @Override
    public Product findOne(long id) {
        LOG.debug("findOne id=" + id);

        return productRepository.findOne(id);
    }

    @Override
    public Page<Product> findAll(Pageable pageable) {
        LOG.debug("findAll");

        return productRepository.findAll(pageable);
    }

    @Override
    public Page<Product> findByNameLike(Pageable pageable, String name) {
        LOG.debug("findByName name=" + name);

        return productRepository.findByNameLike(pageable, name);
    }

}

package com.izulrad.btlabtest3.service;

import com.izulrad.btlabtest3.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProductService {

    Product save(Product product);

    void delete(long id);

    Product findOne(long id);

    Page<Product> findAll(Pageable pageable);

    Page<Product> findByNameLike(Pageable pageable, String name);

}

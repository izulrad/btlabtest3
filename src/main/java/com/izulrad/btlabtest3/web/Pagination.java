package com.izulrad.btlabtest3.web;

import java.util.Collection;

public class Pagination {

    private Page previous;
    private Page next;
    private Collection<Page> pages;
    private String sortByName;

    public Page getPrevious() {
        return previous;
    }

    public void setPrevious(Page previous) {
        this.previous = previous;
    }

    public Page getNext() {
        return next;
    }

    public void setNext(Page next) {
        this.next = next;
    }

    public Collection<Page> getPages() {
        return pages;
    }

    public void setPages(Collection<Page> pages) {
        this.pages = pages;
    }

    public String getSortByName() {
        return sortByName;
    }

    public void setSortByName(String sortByName) {
        this.sortByName = sortByName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pagination that = (Pagination) o;

        if (previous != null ? !previous.equals(that.previous) : that.previous != null) return false;
        if (next != null ? !next.equals(that.next) : that.next != null) return false;
        if (pages != null ? !pages.equals(that.pages) : that.pages != null) return false;
        return !(sortByName != null ? !sortByName.equals(that.sortByName) : that.sortByName != null);

    }

    @Override
    public int hashCode() {
        int result = previous != null ? previous.hashCode() : 0;
        result = 31 * result + (next != null ? next.hashCode() : 0);
        result = 31 * result + (pages != null ? pages.hashCode() : 0);
        result = 31 * result + (sortByName != null ? sortByName.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Pagination{" +
                "previous=" + previous +
                ", next=" + next +
                ", pages=" + pages +
                ", sortByName='" + sortByName + '\'' +
                '}';
    }

    public static class Page {

        private final  String name;
        private final  String link;
        private boolean active;

        public Page(String name, String link) {
            this.name = name;
            this.link = link;
        }

        public String getName() {
            return name;
        }

        public String getLink() {
            return link;
        }

        public boolean isActive() {
            return active;
        }

        public void setActive(boolean active) {
            this.active = active;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Page page = (Page) o;

            if (active != page.active) return false;
            if (name != null ? !name.equals(page.name) : page.name != null) return false;
            return !(link != null ? !link.equals(page.link) : page.link != null);

        }

        @Override
        public int hashCode() {
            int result = name != null ? name.hashCode() : 0;
            result = 31 * result + (link != null ? link.hashCode() : 0);
            result = 31 * result + (active ? 1 : 0);
            return result;
        }

        @Override
        public String toString() {
            return "Page{" +
                    "name='" + name + '\'' +
                    ", link='" + link + '\'' +
                    ", active=" + active +
                    '}';
        }
    }
}

package com.izulrad.btlabtest3.web;


import com.izulrad.btlabtest3.web.Pagination.Page;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.List;

public class WebUtil {
    static final Logger LOG = LoggerFactory.getLogger(WebUtil.class);

    private static final int PAGES_LINK_COUNT = 5;

    /**
     * @param url   - target url
     * @param key   - key for insertion or replacement
     * @param value - value of key
     */
    public static String replaceOrInsertIntoUrl(String url, String key, String value) {
        LOG.debug(String.format("replaceOrInsertIntoUrl url=%s key=%s value=%s", url, key, value));

        if (url.contains(key)) return url.replaceAll(key + "=[^&]+", key + "=" + value);
        else return url + (url.contains("?") ? "&" : "?") + key + "=" + value;
    }

    /**
     * @param url           - current url
     * @param pageable      - current pageable
     * @param totalElements - total elements for pagination
     */
    public static Pagination generatePagination(String url, Pageable pageable, long totalElements) {
        int pageNumber = pageable.getPageNumber();
        int pageSize = pageable.getPageSize();

        LOG.debug(String.format("generatePagination url=%s pageNumber=%d pageSize=%d totalElements=%d",
                url, pageNumber, pageSize, totalElements));

        int totalPages = (int) Math.ceil((double) totalElements / (double) pageSize);

        Pagination pagination = new Pagination();

        if (pageNumber > 0) pagination.setPrevious(createPage(url, "previous", pageNumber - 1));
        if (pageNumber + 1 < totalPages) pagination.setNext(createPage(url, "next", pageNumber + 1));

        List<Page> pages = new ArrayList<>();
        pagination.setPages(pages);


        int firstPageNumber = totalPages - PAGES_LINK_COUNT < pageNumber - 2 ?
                totalPages - PAGES_LINK_COUNT : pageNumber - 2;

        for (int i = firstPageNumber; i < totalPages && pages.size() < 5; i++) {
            if (i >= 0) {
                Page page = createPage(url, (i + 1) + "", i);
                if (i == pageNumber) page.setActive(true);
                pages.add(page);
            }
        }

        Sort sort = pageable.getSort();
        String sortKeyValue;
        if (sort != null) {
            sortKeyValue = sort.getOrderFor("name").isAscending() ? "name,desc" : "name,asc";
        } else {
            sortKeyValue = "name,desc";
        }
        pagination.setSortByName(replaceOrInsertIntoUrl(url, "sort", sortKeyValue));

        return pagination;
    }

    private static Page createPage(String url, String name, int pageNumber) {
        LOG.debug("createPage name=" + name + " pageNumber=" + pageNumber);

        return new Page(name, replaceOrInsertIntoUrl(url, "page", pageNumber + ""));
    }

}

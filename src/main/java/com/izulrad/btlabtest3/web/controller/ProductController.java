package com.izulrad.btlabtest3.web.controller;

import com.izulrad.btlabtest3.model.Product;
import com.izulrad.btlabtest3.service.ProductService;
import com.izulrad.btlabtest3.web.Pagination;
import com.izulrad.btlabtest3.web.WebUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
public class ProductController {

    static final Logger LOG = LoggerFactory.getLogger(ProductController.class);

    private ProductService productService;

    @Autowired
    public void setProductService(ProductService productService) {
        LOG.debug("setProductService");
        this.productService = productService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String main() {
        LOG.debug("main");
        return "redirect:/list";
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String listProducts(ModelMap model, Pageable pageable, HttpServletRequest request,
                               @RequestParam(name = "name", required = false) String name) {
        LOG.debug("listProducts");

        Page<Product> productPage = (name == null) ?
                productService.findAll(pageable) : productService.findByNameLike(pageable, name);

        String url = request.getRequestURL().toString();
        if (request.getQueryString() != null) url += "?" + request.getQueryString();

        Pagination pagination = WebUtil.generatePagination(url, pageable, productPage.getTotalElements());

        model.addAttribute("products", productPage.getContent());
        model.addAttribute("pagination", pagination);

        return "/productList";
    }

    @RequestMapping(value = "/product/{id}", method = RequestMethod.GET)
    public String product(ModelMap model, @PathVariable long id) {
        LOG.debug("product id=" + id);

        model.put("product", productService.findOne(id));
        return "/productForm";
    }

    @RequestMapping(value = "/product/new", method = RequestMethod.GET)
    public String newProduct(ModelMap model) {
        LOG.debug("newProduct");

        model.put("product", new Product());
        return "/productForm";
    }

    @RequestMapping(value = "/product/{id}/delete", method = RequestMethod.POST)
    public ResponseEntity deleteProduct(@PathVariable long id) {
        LOG.debug("deleteProduct id=" + id);

        productService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/product/save", method = RequestMethod.POST)
    public String saveProduct(Product product) {
        LOG.debug("saveProduct " + product);

        productService.save(product);
        return "redirect:/list";
    }
}
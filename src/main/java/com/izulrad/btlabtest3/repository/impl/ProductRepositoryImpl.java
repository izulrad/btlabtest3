package com.izulrad.btlabtest3.repository.impl;

import com.izulrad.btlabtest3.model.Product;
import com.izulrad.btlabtest3.repository.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public class ProductRepositoryImpl implements ProductRepository {
    static final Logger LOG = LoggerFactory.getLogger(ProductRepositoryImpl.class);

    private ProxyProductRepository repository;

    @Autowired
    public void setRepository(ProxyProductRepository repository) {
        LOG.debug("setRepository");

        this.repository = repository;
    }

    @Override
    public Product save(Product product) {
        LOG.debug("save " + product);

        return repository.save(product);
    }

    @Override
    public Product findOne(long id) {
        LOG.debug("findOne id=" + id);

        return repository.findOne(id);
    }

    @Override
    public void delete(long id) {
        LOG.debug("delete id=" + id);

        repository.delete(id);
    }

    @Override
    public Page<Product> findAll(Pageable pageable) {
        LOG.debug("findAll");

        return repository.findAll(pageable);
    }

    @Override
    public Page<Product> findByNameLike(Pageable pageable, String name) {
        LOG.debug("findByNameLike name=" + name);

        return repository.findByNameLike(pageable, name);
    }
}

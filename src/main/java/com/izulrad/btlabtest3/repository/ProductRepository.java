package com.izulrad.btlabtest3.repository;

import com.izulrad.btlabtest3.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProductRepository {

    Product save(Product product);

    Product findOne(long id);

    void delete(long id);

    Page<Product> findAll(Pageable pageable);

    Page<Product> findByNameLike(Pageable pageable, String name);

}

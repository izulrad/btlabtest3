package com.izulrad.btlabtest3.common;

import com.izulrad.btlabtest3.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.*;

public class ProductHelper {
    public static final Date date;
    public static final String dateString = "20.02.2016";

    static {
        Calendar instance = GregorianCalendar.getInstance();
        instance.set(2016, Calendar.FEBRUARY, 20, 0, 0, 0);
        date = instance.getTime();
    }

    public static final Product product = new Product(0L, "foo", "bar", date, 1, true);
    public static final Product savedProduct = new Product(1L, "foo", "bar", date, 1, true);
    public static final List<Product> productList = Arrays.asList(savedProduct,
            new Product(2L, "foo2", "bar2", date, 2, true),
            new Product(3L, "foo3", "bar3", date, 3, false),
            new Product(4L, "foo4", "bar4", date, 4, true),
            new Product(5L, "foo5", "bar5", date, 5, false)
    );
    public static final Pageable defaultPageable = new PageRequest(0, 20);
    public static final Page<Product> defaultPage = new PageImpl<>(productList, defaultPageable, 5);


}

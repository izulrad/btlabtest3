package com.izulrad.btlabtest3.web.controller;

import com.izulrad.btlabtest3.model.Product;
import com.izulrad.btlabtest3.service.ProductService;
import com.izulrad.btlabtest3.web.Pagination;
import com.izulrad.btlabtest3.web.WebUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import javax.annotation.Resource;

import static com.izulrad.btlabtest3.common.ProductHelper.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration({
        "file:src/main/resources/config/spring-app.xml",
        "file:src/main/resources/config/spring-mvc.xml",
        "file:src/main/resources/config/spring-db.xml"
})
public class ProductControllerTest {

    private MockMvc mockMvc;

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    protected WebApplicationContext wac;

    @Mock
    private ProductService productService;

    @InjectMocks
    @Resource
    private ProductController controller;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = webAppContextSetup(this.wac).build();
    }

    @Test
    public void testMain() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/list"));
    }

    @Test
    public void testListProducts() throws Exception {
        String host = "http://localhost";
        String path = "/list";
        String search = "?page=1&size=1&name=fo";
        int pageNumber = 1;
        int pageSize = 1;
        int totalPages = 5;
        Pageable pageable = new PageRequest(pageNumber, pageSize);
        Pagination pagination = WebUtil.generatePagination(host + path + search, new PageRequest(pageNumber, pageSize), totalPages);

        when(productService.findAll(defaultPageable)).thenReturn(defaultPage);
        mockMvc.perform(get(path))
                .andExpect(status().isOk())
                .andExpect(view().name("/productList"))
                .andExpect(model().attribute("products", defaultPage.getContent()));


        when(productService.findByNameLike(pageable, "fo")).thenReturn(defaultPage);
        mockMvc.perform(get(path + search))
                .andExpect(status().isOk())
                .andExpect(view().name("/productList"))
                .andExpect(model().attribute("products", defaultPage.getContent()))
                .andExpect(model().attribute("pagination", pagination));
    }

    @Test
    public void testProduct() throws Exception {
        when(productService.findOne(1L)).thenReturn(savedProduct);
        mockMvc.perform(get("/product/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("/productForm"))
                .andExpect(model().attribute("product", savedProduct));
    }

    @Test
    public void testNewProduct() throws Exception {
        mockMvc.perform(get("/product/new"))
                .andExpect(status().isOk())
                .andExpect(view().name("/productForm"))
                .andExpect(model().attribute("product", new Product()));
    }

    @Test
    public void testDeleteProduct() throws Exception {
        mockMvc.perform(post("/product/1/delete"))
                .andExpect(status().isOk());

        verify(productService, times(1)).delete(1L);
    }

    @Test
    public void testSaveProduct() throws Exception {
        MultiValueMap<String, String> form = new LinkedMultiValueMap<>();
        form.add("id", "0");
        form.add("name", "foo");
        form.add("description", "bar");
        form.add("createdDate", dateString);
        form.add("placeStorage", "1");
        form.add("_reserved", "on");
        form.add("reserved", "on");

        when(productService.save(product)).thenReturn(savedProduct);
        mockMvc.perform(post("/product/save").params(form))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/list"));

        verify(productService, times(1)).save(product);
    }
}
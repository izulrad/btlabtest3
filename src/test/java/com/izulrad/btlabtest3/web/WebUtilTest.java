package com.izulrad.btlabtest3.web;


import org.junit.Test;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;


public class WebUtilTest {

    @Test
    public void testReplaceOrInsertIntoUrl() throws Exception {
        assertEquals(
                "Should replace page to 5",
                "http://localhost:8080/list?page=5&size=10",
                WebUtil.replaceOrInsertIntoUrl("http://localhost:8080/list?page=1&size=10", "page", "5")
        );

        assertEquals(
                "Should add &page=5",
                "http://localhost:8080/list?size=10&page=5",
                WebUtil.replaceOrInsertIntoUrl("http://localhost:8080/list?size=10", "page", "5")
        );

        assertEquals(
                "Should add ?page=5",
                "http://localhost:8080/list?page=5",
                WebUtil.replaceOrInsertIntoUrl("http://localhost:8080/list", "page", "5")
        );
    }

    @Test
    public void testGeneratePagination() throws Exception {
        String url1 =  "http://localhost:8080/list?page=5&size=10";
        Pagination pagination1 = new Pagination();
        pagination1.setPrevious(new Pagination.Page("previous", "http://localhost:8080/list?page=4&size=10"));
        pagination1.setNext(new Pagination.Page("next", "http://localhost:8080/list?page=6&size=10"));
        pagination1.setSortByName("http://localhost:8080/list?page=5&size=10&sort=name,desc");

        List<Pagination.Page> pages1 = new ArrayList<>();
        pages1.add(new Pagination.Page("4", "http://localhost:8080/list?page=3&size=10"));
        pages1.add(new Pagination.Page("5", "http://localhost:8080/list?page=4&size=10"));
        Pagination.Page current1 = new Pagination.Page("6", "http://localhost:8080/list?page=5&size=10");
        current1.setActive(true);
        pages1.add(current1);
        pages1.add(new Pagination.Page("7", "http://localhost:8080/list?page=6&size=10"));
        pages1.add(new Pagination.Page("8", "http://localhost:8080/list?page=7&size=10"));
        pagination1.setPages(pages1);

        assertEquals(
                "Should generate pagination 1",
                pagination1,
                WebUtil.generatePagination(url1, new PageRequest(5, 10), 100)
        );

        String url2 =  "http://localhost:8080/list?page=0&size=10";
        Pagination pagination2 = new Pagination();
        pagination2.setPrevious(null);
        pagination2.setNext(new Pagination.Page("next", "http://localhost:8080/list?page=1&size=10"));
        pagination2.setSortByName("http://localhost:8080/list?page=0&size=10&sort=name,asc");

        List<Pagination.Page> pages2 = new ArrayList<>();
        Pagination.Page current2 = new Pagination.Page("1", "http://localhost:8080/list?page=0&size=10");
        current2.setActive(true);
        pages2.add(current2);
        pages2.add(new Pagination.Page("2", "http://localhost:8080/list?page=1&size=10"));
        pages2.add(new Pagination.Page("3", "http://localhost:8080/list?page=2&size=10"));
        pages2.add(new Pagination.Page("4", "http://localhost:8080/list?page=3&size=10"));
        pages2.add(new Pagination.Page("5", "http://localhost:8080/list?page=4&size=10"));
        pagination2.setPages(pages2);

        Sort sort = new Sort(Sort.Direction.DESC, "name");
        PageRequest pageable = new PageRequest(0, 10, sort);

        assertEquals(
                "Should generate pagination 2",
                pagination2,
                WebUtil.generatePagination(url2, pageable, 100)
        );
    }

}
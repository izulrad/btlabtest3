package com.izulrad.btlabtest3.service.impl;

import com.izulrad.btlabtest3.repository.ProductRepository;
import org.junit.Before;
import org.junit.Test;

import static com.izulrad.btlabtest3.common.ProductHelper.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class ProductServiceImplTest {

    private ProductServiceImpl service;
    private ProductRepository repositoryMock;

    @Before
    public void setUp() throws Exception {
        repositoryMock = mock(ProductRepository.class);
        service = new ProductServiceImpl();
        service.setProductRepository(repositoryMock);
    }

    @Test
    public void testSave() throws Exception {
        when(repositoryMock.save(product)).thenReturn(savedProduct);
        assertEquals("should save product and return it", savedProduct, service.save(product));
        verify(repositoryMock, times(1)).save(product);
    }

    @Test
    public void testDelete() throws Exception {
        service.delete(1L);
        verify(repositoryMock, times(1)).delete(1L);
    }

    @Test
    public void testFindOne() throws Exception {
        when(repositoryMock.findOne(1L)).thenReturn(savedProduct);
        assertEquals("should return product where id=1L", savedProduct, service.findOne(1L));
        verify(repositoryMock, times(1)).findOne(1L);
    }

    @Test
    public void testFindAll() throws Exception {
        when(repositoryMock.findAll(defaultPageable)).thenReturn(defaultPage);
        assertEquals("should return defaultPage of product", defaultPage, service.findAll(defaultPageable));
        verify(repositoryMock, times(1)).findAll(defaultPageable);
    }

    @Test
    public void testFindByNameLike() throws Exception {
        String name = "fo";
        when(repositoryMock.findByNameLike(defaultPageable, name)).thenReturn(defaultPage);
        assertEquals("should return defaultPage of product by name", defaultPage, service.findByNameLike(defaultPageable, name));
        verify(repositoryMock, times(1)).findByNameLike(defaultPageable, name);
    }
}
package com.izulrad.btlabtest3.repository.impl;

import org.junit.Before;
import org.junit.Test;

import static com.izulrad.btlabtest3.common.ProductHelper.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class ProductRepositoryImplTest {

    private ProductRepositoryImpl repository;
    private ProxyProductRepository proxyRepoMock;


    @Before
    public void setUp() throws Exception {
        proxyRepoMock = mock(ProxyProductRepository.class);
        repository = new ProductRepositoryImpl();
        repository.setRepository(proxyRepoMock);
    }

    @Test
    public void testSave() throws Exception {
        when(proxyRepoMock.save(product)).thenReturn(savedProduct);
        assertEquals("should save product and return with id=1L", savedProduct, repository.save(product));
        verify(proxyRepoMock, times(1)).save(product);
    }

    @Test
    public void testFindOne() throws Exception {
        when(proxyRepoMock.findOne(1L)).thenReturn(savedProduct);
        assertEquals("should return product where id=1L", savedProduct, repository.findOne(1L));
        verify(proxyRepoMock, times(1)).findOne(1L);
    }

    @Test
    public void testDelete() throws Exception {
        repository.delete(1L);
        verify(proxyRepoMock, times(1)).delete(1L);
    }

    @Test
    public void testFindAll() throws Exception {
        when(proxyRepoMock.findAll(defaultPageable)).thenReturn(defaultPage);
        assertEquals("should return defaultPage of product", defaultPage, repository.findAll(defaultPageable));
        verify(proxyRepoMock, times(1)).findAll(defaultPageable);
    }

    @Test
    public void testFindByNameLike() throws Exception {
        String name = "foo";
        when(proxyRepoMock.findByNameLike(defaultPageable, name)).thenReturn(defaultPage);
        assertEquals("should return defaultPage of product by name", defaultPage, repository.findByNameLike(defaultPageable, name));
        verify(proxyRepoMock, times(1)).findByNameLike(defaultPageable, name);
    }
}
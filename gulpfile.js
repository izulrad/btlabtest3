var
  rimraf = require('gulp-rimraf'),
  uglify = require('gulp-uglify'),
  cssnano = require('gulp-cssnano'),
  concat = require('gulp-concat'),
  gulp = require('gulp');

var
  vendorJs = [
    'bower_components/jquery/dist/jquery.js',
    'web/js/vendor/*.js',
    'bower_components/bootstrap/dist/js/bootstrap.js'
  ],
  vendorCss = [
    'web/css/vendor/*.css',
    'bower_components/bootstrap/dist/css/bootstrap.css'
  ],

  myJs = ['web/js/my/*.js'],
  myCss = ['web/css/my/*.css'],
  myFtl = ['web/ftl/my/*.ftl'],
  myFtlMacros = ['web/ftl/my/macros/*.ftl'],

  distPath = 'src/main/webapp/WEB-INF',
  distPathView = distPath + '/view',
  distPathMacros = distPathView + '/macros',
  distPathStatic = distPath + '/static',
  distPathJs = distPathStatic + '/js',
  distPathCss = distPathStatic + '/css'
  ;

gulp.task('copy:concat:uglify:js', function () {
  return gulp.src(vendorJs.concat(myJs))
    .pipe(uglify())
    .pipe(concat("scripts.min.js"))
    .pipe(gulp.dest(distPathJs))
});

gulp.task('copy:concat:minify:css', function () {
  return gulp.src(vendorCss.concat(myCss))
    .pipe(cssnano())
    .pipe(concat("styles.min.css"))
    .pipe(gulp.dest(distPathCss))
});

gulp.task('copy:ftl', function () {
  return gulp.src(myFtl)
    .pipe(gulp.dest(distPathView));
});

gulp.task('copy:ftl_macros', function () {
  return gulp.src(myFtlMacros)
    .pipe(gulp.dest(distPathMacros));
});

gulp.task('clean:distPath', function () {
  return gulp.src([distPathView, distPathStatic],  {read: false})
    .pipe(rimraf());
});

gulp.task('build', ['clean:distPath'], function () {
  gulp.start('copy:concat:uglify:js', 'copy:concat:minify:css', 'copy:ftl', 'copy:ftl_macros');
});

gulp.task('dev', ['build'], function () {
  gulp.watch(myJs, ['copy:concat:uglify:js']);
  gulp.watch(myCss, ['copy:concat:minify:css']);
  gulp.watch(myFtl, ['copy:ftl']);
  gulp.watch(myFtlMacros, ['copy:ftl_macros']);
});
### Тестовое задание для BTLab ###

# README #

`mvn clean package` и в `/target` можно забрать war файл

Версия должна быть не ниже Maven version >=3.1.0.

# FRONT #

Для работы необходимы следующие инструменты:

1. [Nodejs](https://nodejs.org/) инкструкция для nix [тут](http://competa.com/blog/2014/12/how-to-run-npm-without-sudo/)
2. [Bower](http://bower.io/) `$ npm install --global bower` 
3. [Gulp](http://gulpjs.com/) `$ npm install --global gulp`
4. `$ gulp dev` - соберет проект, повесит ватчеры на сорс файлы, будет пересобирать web если файлы изменились

Первый запуск(или запуск после обновления зависимостей):

1. `$ npm install` - установит в `node_modules`
2. `$ bower install` - установит в `bower_components`

# Используемые технологии

* back - `spring-webmvc`, `spring-data-jpa`, `hibernate`, `jUnit`, `mockito`

* front - `npm`, `bower`, `gulp`, `freemarker`, `jquery`, `bootstrap 3`
